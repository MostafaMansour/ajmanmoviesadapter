/*
 *  Licensed Materials - Property of IBM
 *  5725-I43 (C) Copyright IBM Corp. 2011, 2016. All Rights Reserved.
 *  US Government Users Restricted Rights - Use, duplication or
 *  disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */

/**
 * @param tag: a topic such as MobileFirst_Platform, Bluemix, Cordova.
 * @returns json list of items.
 */

function getFeed(tag) {
	var input = {
	    method : 'get',
	    returnedContentType : 'xml',
	    path : getPath(tag)
	};

	return MFP.Server.invokeHttp(input);
}


function filmsNowShowing() {
	var input = {
	    method : 'get',
	    returnedContentType : 'json',
	    path : 'filmsNowShowing',
		headers: this.buildHeader()
	};
	MFP.Logger.info(JSON.stringify(input));
	return MFP.Server.invokeHttp(input);
}

function filmsComingSoon() {
	var input = {
	    method : 'get',
	    returnedContentType : 'json',
	    path : 'filmsComingSoon',
		headers: this.buildHeader()
	};
	MFP.Logger.info(JSON.stringify(input));
	return MFP.Server.invokeHttp(input);
}

function buildHeader(){
    	return {
			"api-version": "v200",
			"Authorization": MFP.Server.getPropertyValue("Authorization"),
			"client": "TAHA",
			"x-api-key": MFP.Server.getPropertyValue("x-api-key"),
			"device-datetime": new Date().toISOString(),
			"territory": MFP.Server.getPropertyValue("territory"),
			"Geolocation": "38.413128, -106.161488",
			"app_version": "1.0",
			"app_platform": "react_native"
 		}
}

function getPath(tag){
    if(tag === undefined || tag === ''){
        return 'feed.xml';
    } else {
        return 'blog/atom/' + tag + '.xml';
    }
}

/**
 * @returns ok
 */
function unprotected(param) {
	return {result : "Hello from unprotected resource"};
}